Miscmarketv4::Application.routes.draw do

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :registrations => "registrations"  } do
    get 'sign_in', :to => 'users/sessions#new', :as => :new_user_session
    get 'sign_out', :to => 'users/sessions#destroy', :as => :destroy_user_session
  end

  # User
  match 'Bubble/:user_id' => 'users#index', :as=>'user_home'
  #match '/users/:user_id/welcome' => 'users#welcome', :as=>'user_welcome_home'
  match '/users/:user_id/verify' => 'users#verify', :as=>'user_verify'
  match '/users/:user_id/social' => 'users#social', :as=>'user_social_add'
  match '/users/:user_id/market/add' => 'markets#add', :as=>'user_market_add'
  match '/users/:user_id/market/confirm' => "markets#confirm", :as => 'user_market_confirm'
  match '/users/:user_id/market/reconfirm' => "markets#reconfirm", :as => 'user_market_reconfirm'
  match '/users/:user_id/markets/verify/:token' => "markets#verify", :as => 'user_market_verify'
  match '/users/:user_id/location/add' => "locations#add", :as => 'user_location_add'
  match '/users/:user_id/location/add/signup' => "locations#sign_up_create", :as => 'user_location_signup_create'
  match '/confirm' => "users#confirm", :as => "user_confirm"
  match '/reconfirm' => "users#reconfirm", :as => "user_reconfirm"

    # iOS
  match '/users/mobile_login', :to => 'users#mobile_login'
  #, :constraints => { :protocol => "https" }
  # http://stackoverflow.com/questions/3634100/rails-3-ssl-deprecation #http://www.simonecarletti.com/blog/2011/05/configuring-rails-3-https-ssl/
  # ssl_required :login # https://github.com/bartt/ssl_requirement

  # User Management
  resources :users

  # Post Management
  resources :posts do
    resources :addresses
    member do
      post :message
    end
  end
  match '/search' => 'posts#search', :as=>'posts_search'

  # Market
  resources :markets

  resources :reposts, :only => [:create]

  # gmail Account Authentication
  match '/gmail_accounts/callback' => 'gmail_accounts#callback', :as => 'gmail_auth_callback'
  match '/gmail_accounts/filter_created' => 'gmail_accounts#filter_created', :as => 'gmail_filter_created'
  resources :gmail_accounts, :only => [:new, :create]

  # Admin
  resources :categories
  resources :locations

  # Session Management for iPhone app
  match '/sessions(.:format)' => 'sessions#create', :as=>'session_create'

  # Miscmarket information sites
  match '/contact' => 'application#contact', :as=>'miscmarket_contact'
  match '/about' => 'application#about', :as=>'miscmarket_about'
  match '/team' => 'application#team', :as=>'miscmarket_team'
  match '/privacy' => 'application#privacy', :as=>'miscmarket_privacy'
  match '/terms' => 'application#terms', :as=>'miscmarket_terms'
  match '/tshirt' => 'application#tshirt', :as=>'miscmarket_tshirt'
  match '/order' => 'application#order', :as=>'miscmarket_tshirt_order'

  # Roots
  match '/' => 'application#index', :as => "home"
  root :to => "application#index", :as => 'root'

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
