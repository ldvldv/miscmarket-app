# iOS requires custom failure http://stackoverflow.com/questions/6474532/http-authentication-between-devise-and-iphone-app/6483222#6483222
class CustomFailure < Devise::FailureApp     
  def respond
   	unless request.format.to_sym == :html
  		http_auth
    else
  		super
	end
  end
end