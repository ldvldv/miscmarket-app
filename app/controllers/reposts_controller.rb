class RepostsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorized_user

  def create
   @repost = Repost.new(params[:repost])
   @post.updated_at = Time.new

    respond_to do |format|
      if @repost.save and @post.update_attributes(params[:post])
        format.html { redirect_to user_home_path(current_user), notice: 'RePost was successful.' }
        format.json { render json: @repost }
      else
        format.html { redirect_to user_posts_path, notice: 'RePost Failed.' }
        format.json { render json: @repost.errors }
      end
    end
  end

private

  def authorized_user
   @post = current_user.posts.find_by_id((params[:repost])[:post_id])
   redirect_to user_home_path(current_user) if @post.nil?
  end
end
