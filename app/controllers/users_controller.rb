class UsersController < ActionController::Base
  layout :resolve_layout
  before_filter :authenticate_user!

  def index
    if current_user.state == nil
      if current_user.fb_id
        redirect_to user_social_add_path(current_user)
      else
        redirect_to user_market_add_path(current_user)
      end
    else
      @categories = Category.all
      #@myPosts = current_user.markets.first.posts.where(:available=>"1").order('created_at DESC').limit(50)

      # @posts = Market.find(current_user.markets.first).posts.where(:available=>"1").order('created_at DESC').limit(50)
      @posts = Post.all
    end
  end

  # def mobile_login
  #   current_user.create_token

  #   respond_to do |format|
  #     format.json {render :text => "#{current_user.id}, #{current_user.authentication_token}" }
  #   end
  # end

  def social
    if !current_user.confirmed?
      redirect_to user_verify_path
    elsif current_user.state != 2
      # not finished sign up process
      if current_user.fb_id == "" && current_user.state != 1
        # never added social yet
        current_user.state = 1
        current_user.save
      elsif current_user.fb_id == "" && current_user.state == 1
        # does not want to use fb
        redirect_to user_market_add_path(current_user) # decided not to use fb
      end
    else
      redirect_to user_home_path(current_user)
    end
  end

  def verify
    if current_user.confirmed?
      if current_user.fb_id == ''
        redirect_to user_social_add_path(current_user)
      else
        redirect_to user_market_add_path(current_user)
      end
    end
  end

  def confirm
    if current_user && current_user.confirmed?
      if(current_user.memberships.size == 0)
        if current_user.fb_id == nil
          redirect_to user_market_facebook_path(current_user)
        else
          redirect_to user_market_path(current_user, current_user.markets.first)
        end
      else
        redirect_to user_market_path(current_user, current_user.markets.first)
      end
    else
      redirect_to home_path
    end
  end

  def reconfirm
    if !current_user.confirmed?
      User.find(current_user).send_confirmation_instructions
      redirect_to miscmarket_confirm_path
    end
  end

  private
  def resolve_layout
    case action_name
    when "social"
      "sign_up"
    else
      "user"
    end
  end
end
