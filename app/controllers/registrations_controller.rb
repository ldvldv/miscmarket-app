class RegistrationsController < Devise::RegistrationsController
  protected

  def after_sign_up_path_for(resource)
    # If facebook added already go to
    if current_user.fb_id == nil
       user_verify_path(resource)
    else
       user_social_add_path(resource)
    end
  end
end
