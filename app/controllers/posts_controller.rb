class PostsController < ApplicationController
  respond_to :html, :json
  before_filter :authenticate_user!
  include ExtraControllerMethods

  # Constants
  AVAILABLE = 1
  EXPIRED = 2
  UNAVAILABLE = 3

  # GET /posts
  # GET /posts.json
  def index
    @error = 0
    sql_condition = 'id IN (SELECT markets_posts.post_id FROM markets_posts WHERE markets_posts.market_id IN'
    # Only retrieve those posts that are in current_user's confirmed markets.
    if params.has_key?("markets")
        markets = params[:markets]
        @error = verify_markets(markets)
    else
        markets = current_user.confirmed_market_ids
    end
    sql_condition += '(' + get_array_without_brackets(markets) + '))'

    if params.has_key?("category")
      category = Category.find_by_name(params[:category])
      if category.nil?
        @error = 1
      else
        cat_id = category.id
        sql_condition += ' AND category_id = ' + cat_id.to_s
      end
    end

    if params.has_key?("availability")
      sql_condition += ' AND availability = ' + params[:availability]
    end

    if params.has_key?("type_id")
      sql_condition += ' AND type_id = ' + params[:type_id]
    end

    if params.has_key?("user")
      sql_condition += ' AND user_id = ' + params[:user]
    end

    per_page = 20
    if params.has_key?("per_page")
        per_page = params[:per_page].to_i
        if per_page > 100
            per_page = 100
        end
    end

    page = 1
    if params.has_key?("page")
        page = params[:page].to_i
        if page == 0
            page = 1
        end
    end

    @posts = []
    if @error == 0
      @posts = Post.paginate(:conditions => [sql_condition], :per_page => per_page, :page => page)
    end

    # @posts.each do |post|
    #   post.price = number_to_currency( post.price, :unit => "$", :precision=>0)
    # end

    respond_to do |format|
      if @error == 0
        format.html # index.html.erb
        format.json { render json: @posts }
      else
        format.html #{ render action: "new" }
        format.json { render json: @posts, status: :unprocessable_entity }
      end
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @post = Post.find(params[:id])
    @repost = Repost.new

    respond_to do |format|
      format.js # show.js.erb
      format.html # show.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/new
  # GET /posts/new.json
  def new
    @post = Post.new
    4.times { @post.uploads.build }

    respond_to do |format|
      format.html # new.html.erb
      format.js # new.js.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/1/edit
  def edit
    @post = Post.find(params[:id])
    3.times { @post.uploads.build }

  end

  # POST /posts
  # POST /posts.json
  def create
    category_id = params[:post][:category_id]
    type_id = params[:post][:type_id]
    markets = params[:post][:market_ids]
       
    @post = Post.new(params[:post])
    @post.user_id = current_user.id
    @post.availability = 1

    # Verify if the user is allowed to post to the specified markets.
    # TODO: make verify_markets return an array of the non-allowed markets when false
    if( !current_user.verify_markets(markets) )
      debugger
      @post.errors.add(:market_id, "User not allowed to post to certain markets")
    else
      # Verify if the category and type are valid.
      for market in markets
        count = MarketsCategory.count(:all, :conditions => {:market_id => market, :type_id => type_id, :category_id => category_id})
        if count == 0
          @post.errors.add(:market_id, "#{market} has no #{category_id} with #{type_id}")
        end
      end
    end

    respond_to do |format|
      if @post.save
        format.html { redirect_to posts_path, notice: 'Post was successfully created.' }
        format.json { render json: @post, status: :created, location: @post }
        format.js { @post.errors }
      else
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
        format.js { @post.errors }
      end
    end
  end

  # PUT /posts/1
  # PUT /posts/1.json
  def update
    @post = Post.find(params[:id])

    if @post.update_attributes(params[:post])
      render json: @post
    else
      render json: @post.errors, status: :unprocessable_entity
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.html { redirect_to user_posts_path }
      format.json { head :ok }
    end
  end

  # GET /search
  # GET /search.json
  def search
    error = 0
    if params.has_key?("q")
        query = params[:q]
    else
        error = 1
    end

    conditions = Hash.new
    # By default, only check for posts that available.
    if params.has_key?("availability")
        conditions[:availability] = params[:availability]
    else
        conditions[:availability] = AVAILABLE
    end

    if params.has_key?("page")
        page = params[:page]
    else
        page = 1
    end

    if params.has_key?("per_page")
        per_page = params[:per_page]
    else
        per_page = 10
    end

    search_query = SearchQuery.new(:query => query,
                                   :availability => conditions[:availability],
                                   :page => page, :per_page => per_page,
                                   :user_id => current_user.id)

    if params.has_key?("category_id")
        conditions[:category_id] = params[:category_id]
        search_query.category_id = params[:category_id]
    end

    if params.has_key?("type_id")
        conditions[:type_id] = params[:type_id]
        search_query.type_id = params[:type_id]
    end

    if params.has_key?("state_id")
        conditions[:state_id] = params[:state_id]
        search_query.state_id = params[:state_id]
    end

    if params.has_key?("user_id")
        conditions[:user_id] = params[:user_id]
        search_query.user_id_searched = params[:user_id]
    end

    # If not specified, check in all the user's markets.
    if params.has_key?("market_ids")
        markets = params[:market_ids]
        @error = verify_markets(markets)
    else
        markets = current_user.confirmed_market_ids
    end
    search_query.market_ids = markets
    markets = get_array_without_brackets(markets)

    if error == 0
        @search_results = Post.visible(markets).search_tank(
            query, :page => page, :per_page => per_page,
            :snippets => [:description], :conditions => conditions,
            :fetch => [:title, :price, :type_id, :category_id, :availability,
                       :state_id, :user_id, :description])
        num = @search_results.length
        i = 0
        while i < num
            if @search_results[i].description_snippet != ""
                @search_results[i][:description] =
                    @search_results[i].description_snippet
            end
            i = i + 1
        end
    end

    search_query.error = error
    search_query.result_json = @search_results.to_json
    search_query.save!
    respond_to do |format|
      if error == 0
        format.html
        format.json { render json: @search_results }
      else
        format.html
        format.json { render json: @search_results, status: :unprocessable_entity }
      end
    end
  end

  def message
    # @sender = User.first.email
    # To " + @email + " From " + @from
    @post = Post.find(params[:id])
    @email = User.find(@post.user_id).email
    @sender = User.find(current_user).email
    @message = params[:message_content] + "\n\nRegarding post:\n" + @post.title + "\n\n" + @post.description

    # TODO
    if(@message == "")
      # redirect_to(user_market_path(current_user, current_user.markets.first), :notice => "Message can't be empty")
    else
      Message.messenger(@email, @sender, @message).deliver
      # redirect_to(user_market_path(current_user, current_user.markets.first), :notice => 'Message was successfully sent')
    end
  end
end
