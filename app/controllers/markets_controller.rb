class MarketsController < ApplicationController
  http_basic_authenticate_with :name => "ldevalliere", :password => "miscmarket123", :except => [:add, :confirm, :reconfirm, :verify, :index]

  layout 'user'
  before_filter :authenticate_user!

  # GET /markets
  # GET /markets.json
  def index
    @markets = current_user.confirmed_markets

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @markets }
    end
  end

  # GET /markets/1
  # GET /markets/1.json
  def show
    @market = Market.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @market }
    end
  end

  # GET /markets/new
  # GET /markets/new.json
  def new
    @market = Market.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @market }
    end
  end

  # GET /markets/1/edit
  def edit
    @market = Market.find(params[:id])
  end

  # POST /markets
  # POST /markets.json
  def create
    @market = Market.new(params[:market])

    respond_to do |format|
      if @market.save
        format.html { redirect_to @market, notice: 'Market was successfully created.' }
        format.json { render json: @market, status: :created, location: @market }
      else
        format.html { render action: "new" }
        format.json { render json: @market.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /markets/1
  # PUT /markets/1.json
  def update
    @market = Market.find(params[:id])

    respond_to do |format|
      if @market.update_attributes(params[:market])
        format.html { redirect_to @market, notice: 'Market was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @market.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /markets/1
  # DELETE /markets/1.json
  def destroy
    @market = Market.find(params[:id])
    @market.destroy

    respond_to do |format|
      format.html { redirect_to markets_url }
      format.json { head :ok }
    end
  end

  def add
    if current_user.fb_id == "" && current_user.state == nil
        redirect_to user_social_add_path(current_user)
    else
      @markets = Market.all
      @markets = @markets.sort! { |a,b| a.name <=> b.name }

      if current_user.fb_token != nil
        #Automatically add Facebook networks
        @rest = Koala::Facebook::RestAPI.new(current_user.fb_token)
        @networks = @rest.fql_query("select affiliations from user where uid = me()")

        @networks[0]['affiliations'].each do |network|
          @markets.each do|market|
            if(network['name'] == market.name)
              checker = false;

              current_user.memberships.each do|membership| #check that not already a member
                if(Market.find(membership.market_id).name == market.name)
                  checker = true;
                end
              end

              if checker == false
                current_user.memberships.create(:market_id => market.id, :role => 0, :confirmed_at => Time.now)
              end
            end
          end
        end
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @markets }
    end
  end

  def confirm
    # Request access to a market defined by the :market_id parameter
    @market = Market.find(params[:market_id])

    # Get the email address
    @email = params[:email]
    @sender = "team@miscmarket.org"
    @token = SecureRandom.hex(32)
    confirmed = false;

    # Make sure email corresponds to particular market
    case @email
    when /(\w+\.)?cmu.edu/i;
      if @market.id != 1 #CMU
      end
    when /(\w+\.)?pitt.edu/i;
      if @market.id != 2 #Pitt
      end
    else
      redirect_to(user_market_add_path(current_user), :notice => 'Your email did not correspond with the selected school') and return
    end

    # Check if not already member
    current_user.memberships.each do|membership|
      if(Market.find(membership.market_id).name == @market.name && membership.confirmed_at != nil)
        redirect_to(user_market_add_path(current_user), :notice => "You are already a confirmed member of the " + @market.market + " market") and return
      end
    end

    # Make sure token is unique
    Membership.all.each do |membership|
      if(membership.confirmation_token == @token)
        @token = SecureRandom.hex(32)
      end
    end

    current_user.memberships.create(:market_id => @market.id, :role => 0, :confirmation_token => @token, :email => @email, :confirmation_sent_at => Time.now)
    @message = "Hi " + current_user.first_name + ",\n\nWe just quickly need you to confirm that you belong to the " + @market.name + " market. Please click on the link below:\n\nhttp://localhost:3000/users/" + current_user.id.to_s + "/markets/verify/" + @token + "\n\nThanks,\nThe MiscMarket Team"

    # Send to a specific email defined by the form
    Message.messenger(@email, @sender, @message).deliver
    redirect_to(user_market_add_path(current_user), :notice => "Verification Email to the " + @market.name + " market was successfully sent")
  end

  def reconfirm
    @market = Market.find(params[:market])
    @sender = "team@miscmarket.org"

    current_user.memberships.each do |membership|
      if @market.id == membership.market_id
        @token = membership.confirmation_token
        @email = membership.email
      else
        @token = nil
        @email = nil
      end
    end

    if @token
      @message = "Hi redo" + current_user.first_name + ",\n\nWe just quickly need you to confirm that you belong to the " + @market.name + " market. Please click on the link below:\n\nhttp://localhost:3000/users/" + current_user.id.to_s + "/markets/verify/" + @token + "\n\nThanks,\nThe MiscMarket Team"

      # Send to a specific email defined by the form
      Message.messenger(@email, @sender, @message).deliver

      redirect_to(user_market_add_path(current_user), :notice => "Re-Verification Email to the " + @market.name + " market was successfully sent")
    else
      redirect_to(user_market_add_path(current_user), :notice => "Re-Verification Email to the " + @market.name + " market failed sent")
    end
  end

  def verify
    @memberships  = current_user.memberships
    @market = nil

    @memberships.all.each do |membership|
      if membership.confirmation_token == params[:token]
        membership.confirmation_token = nil;
        membership.confirmed_at = Time.now
        membership.save
        @market = Market.find(membership.market_id)
      end
    end

    @markets = current_user.markets
    @markets = @markets.sort! { |a,b| a.name <=> b.name }

    redirect_to(user_market_add_path(current_user), :notice => "Successfully joined " + @market.name + "!")
  end
end
