class ApplicationController < ActionController::Base
  protect_from_forgery
  layout 'application'

  def index
    if !current_user.nil?
      if current_user.confirmed?
        redirect_to user_home_path(current_user)
      else
        #wait for confirmation
        redirect_to user_verify_path(current_user)
      end
    else
      # user is not logged in at all.

    end
  end

=begin
  #old code
      if current_user.confirmed?
      if current_user.state == 2
        #has finished sign up view
        redirect_to user_home_path(current_user)
      else
        #not finished sign up
      if current_user.fb_id == "" && current_user.state != 1
        redirect_to user_social_add_path(current_user)
      elsif current_user.memberships.size == 0
        #stil needs to add markets
        redirect_to user_market_add_path(current_user)
      elsif current_user.locations.size == 0
        redirect_to user_location_add_path(current_user)
      else
        current_user.state = 2
        current_user.save
        redirect_to user_home_path(current_user)
      end
    end
  else
    #Give verify sign up view
    redirect_to user_verify_path(current_user)
  end
 end

  def after_sign_in_path_for(resource)
    debugger
    if current_user.fb_id != ""
      debugger
      if current_user.memberships.size == 0
        debugger
        redirect_to user_market_add_path(current_user)
      else
        debugger
        redirect_to user_home_path(current_user)
      end
    else
      redirect_to user_social_add_path(current_user)
    end
  end
  end
=end
end
