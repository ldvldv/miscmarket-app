class GmailAccountsController < ApplicationController
  respond_to :html, :json
  before_filter :authenticate_user!

  # Constants
  CONSUMER_KEY = "anonymous"
  CONSUMER_SECRET = "anonymous"
  SITE = "https://www.google.com"
  REQUEST_TOKEN_PATH = "/accounts/OAuthGetRequestToken"
  ACCESS_TOKEN_PATH = "/accounts/OAuthGetAccessToken"
  OAUTH_CALLBACK = "http://localhost:3000/gmail_accounts/callback/"
  SCOPE = "https://mail.google.com/"

  # GET /gmail_accounts/new
  def new
    @gmail_account = GmailAccount.new
  end

  # POST /gmail_accounts
  def create
    # TODO: Verify e-mail is well-formed.
    @account = GmailAccount.new(params[:gmail_account])
    @account.user_id = current_user.id
    @account.gmail_state_id = 1
    @account.active = false
    # If the save is unsuccessful (perhaps because the e-mail already exists in
    # the database), then we at least have the email stored in a variable.
    email = @account.email
    @account.save

    index = email.index('@')
    domain = nil
    if index
        domain = email[(index + 1)..-1]
    end

    if domain == 'gmail.com' or domain == 'googlemail.com'
        authorize_path = "/accounts/OAuthAuthorizeToken"
    else
        authorize_path = '/a/' + domain + '/OAuthAuthorizeToken'
    end

    consumer = OAuth::Consumer.new(CONSUMER_KEY, CONSUMER_SECRET, {
                               :site => SITE,
                               :request_token_path => REQUEST_TOKEN_PATH,
                               :authorize_path => authorize_path,
                               :access_token_path => ACCESS_TOKEN_PATH})

    rt = consumer.get_request_token({:oauth_callback => OAUTH_CALLBACK},
                                    {:scope => SCOPE})
    session[:rt] = rt
    redirect_to rt.authorize_url
  end

  # GET /gmail_accounts/filter_created
  def filter_created
    account = GmailAccount.find_by_user_id(current_user.id)
    if account
        account.active = true
        account.gmail_state_id = 4
        account.save
    end
  end

  # GET /gmail_accounts/callback
  def callback
    oauth_verifier = params[:oauth_verifier]
    account = GmailAccount.find_by_user_id(current_user.id)
    rt = session[:rt]
    @error = 0
    begin
        access_token = rt.get_access_token(:oauth_verifier => oauth_verifier)
        account.gmail_state_id = 2
    rescue
        @error = 1
        account.gmail_state_id = 3
        account.save
    end

    if @error == 0
        account.oauth_token = access_token.token
        account.oauth_secret = access_token.secret
        account.save
        add_labels(account)
    end

    respond_to do |format|
      if @error == 0
        format.html # index.html.erb
        format.json { render json: "Success" }
      else
        format.html #{ render action: "new" }
        format.json { render json: "Fail", status: :unprocessable_entity }
      end
    end

  end

  def add_labels(account)
    # Connect to the gmail account using given tokens and create new labels.
    gmail = Gmail.connect(:xoauth, account.email,
                          :token    => account.oauth_token,
                          :secret => account.oauth_secret,
                          :consumer_key => CONSUMER_KEY,
                          :consumer_secret => CONSUMER_SECRET)
    gmail.labels.new("MiscMarket")
  end
end
