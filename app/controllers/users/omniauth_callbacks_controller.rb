class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  
  def facebook
    # You need to implement the method below in your model
    @data = User.find_for_facebook_oauth(env["omniauth.auth"], current_user)
    @id = @data['id']
    @token = @data['token']
    user =  User.find_by_fb_id(@id)

    if user != nil
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      sign_in user, :event => :authentication
            
      if user.memberships.size > 0
        if user.confirmed?
          # User is registered and confirmed => user home
          redirect_to user_home_path(user)
        else
          # User is registered but not confirmed => confirm page
          redirect_to user_confirm_path
        end
      else
        # User has no markets yet => join market page
        redirect_to  user_market_add_path(user)
      end
    elsif user_signed_in?
      current_user.fb_id = @id
      current_user.fb_token = @token
      current_user.state = 1 # Done Social 
      current_user.save  
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Facebook"
      
      # User successfully added facebook => join market page
      redirect_to  user_market_add_path(current_user)
    else
      session["devise.facebook_data"] = env["omniauth.auth"]
      # User just authorized facebook => finish registration
      redirect_to new_user_registration_url
    end
  end
end