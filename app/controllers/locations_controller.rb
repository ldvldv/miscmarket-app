class LocationsController < ApplicationController
  layout :resolve_layout
  before_filter :authenticate_user!

  # GET /locations
  # GET /locations.json
  def index
    @locations = Location.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @locations }
    end
  end

  # GET /locations/1
  # GET /locations/1.json
  def show
    @location = Location.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @location }
    end
  end

  # GET /locations/new
  # GET /locations/new.json
  def new
    @location = Location.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @location }
    end
  end

  # GET /locations/1/edit
  def edit
    @location = Location.find(params[:id])
  end

  # POST /locations
  # POST /locations.json
  def create
    @location = Location.new(params[:location])

    respond_to do |format|
      if @location.save
        format.html { redirect_to @location, notice: 'Location was successfully created.' }
        format.json { render json: @location, status: :created, location: @location }
      else
        format.html { render action: "new" }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /locations/1
  # PUT /locations/1.json
  def update
    @location = Location.find(params[:id])

    respond_to do |format|
      if @location.update_attributes(params[:location])
        format.html { redirect_to @location, notice: 'Location was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.json
  def destroy
    @location = Location.find(params[:id])
    @location.destroy

    respond_to do |format|
      format.html { redirect_to locations_url }
      format.json { head :ok }
    end
  end

  def add
    if current_user.markets.size == 0
      redirect_to user_market_add_path, notice: 'You need to join a market first'
    end

    @location = Location.new

    # User has complete sign up process
    current_user.state = 2
    current_user.save

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @location }
    end
  end

  def sign_up_create
    @location = Location.new(params[:location])
    @location.user_id = current_user.id
    @location.available = true

    respond_to do |format|
      if @location.save
        format.html { redirect_to @location, notice: 'Welcome to MiscMarket' }
      else
        format.html { redirect_to user_location_add_path(@location), notice: 'Could not save your address, please check it.'}
      end
    end
  end

  private
  def resolve_layout
    case action_name
    when "add"
      "sign_up"
    else
      "user"
    end
  end
end
