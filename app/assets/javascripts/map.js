// // Load map and initliaze
// window.onload = init;
// 
// // Global variables for map, geocoder and marker
// var map, geocoder, initialLocation;
// 
// function init() {
// 	// Creating an object literal containing the properties
// 	var options = {
// 		zoom: 16, 
// 		center: new google.maps.LatLng(40.4446772, -79.9450306),
// 		mapTypeId: google.maps.MapTypeId.ROADMAP,
// 		mapTypeControl:true,
// 		mapTypeControlOptions:{
// 			style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
// 			mapTypeIds: [ google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE ]
// 		},
// 		disableDefaultUI: true,
// 		navigationControl:true,
// 		navigationControlOptions:{
// 			position: google.maps.ControlPosition.TOP_RIGHT,
// 			style: google.maps.NavigationControlStyle.SMALL
// 		},
// 		scrollwheel:false,
// 		streetViewControl: true,
// 		backgroundColor: '#FFFFFF'
// 	};
// 
// 	// Creating the map 
// 	map = new google.maps.Map(document.getElementById('map_canvas'), options);
// 
// 	// Initiliaze geocoder
// 	if(!geocoder){ 
// 		geocoder = new google.maps.Geocoder();
// 	}
// 
// 	// Try W3C Geolocation (Preferred)
// 	if(navigator.geolocation) {
// 		browserSupportFlag = true;
// 		navigator.geolocation.getCurrentPosition(function(position) {
// 			initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
// 
// 			var infowindow = new google.maps.Marker({
// 				map: map,
// 				position: initialLocation
// 			});
// 
// 			map.setCenter(initialLocation);
// 		}, 
// 		function() {
// 			handleNoGeolocation(browserSupportFlag);
// 		});
// 
// 		// Try Google Gears Geolocation
// 	} else if (google.gears) {
// 		browserSupportFlag = true;
// 		var geo = google.gears.factory.create('beta.geolocation');
// 		geo.getCurrentPosition(function(position) {
// 			initialLocation = new google.maps.LatLng(position.latitude,position.longitude);
// 
// 			var infowindow = new google.maps.Marker({
// 				map: map,
// 				position: initialLocation
// 			});
// 
// 			map.setCenter(initialLocation);
// 		}, function() {
// 			handleNoGeoLocation(browserSupportFlag);
// 		});
// 
// 		// Browser doesn't support Geolocation
// 	} else {
// 		browserSupportFlag = false;
// 		handleNoGeolocation(browserSupportFlag);
// 	}
// 	function handleNoGeolocation(errorFlag) {
// 		if (errorFlag) {
// 			var content = 'Error: The Geolocation service failed.';
// 		} else {
// 			var content = 'Error: Your browser doesn\'t support geolocation.';
// 		}
// 
// 		var options = {
// 			map: map,
// 			position: new google.maps.LatLng(60, 105),
// 			content: content
// 		};
// 
// 		var infowindow = new google.maps.InfoWindow(options);
// 		map.setCenter(options.position);
// 	}
// }; 