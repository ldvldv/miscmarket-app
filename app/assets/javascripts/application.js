//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require_tree .

// Custom JS
var map, geocoder, initialLocation;

var app = {
	basicFunctionality : function(){

	}, 	
	userMainView : function(){
		// Dropdown  
		$("body").bind("click", function (e) {
	    	$('a.menu-left').parent("li").removeClass("open");
	    	$('a.menu-right').parent("li").removeClass("open");
		});
 
		$("a.menu-left").click(function (e) {
			$('a.menu-right').parent("li").removeClass("open");
			var $li = $(this).parent("li").toggleClass('open');
			return false;
		});
		
		$("a.menu-right").click(function (e) {
			$('a.menu-left').parent("li").removeClass("open");
			var $li = $(this).parent("li").toggleClass('open');
			return false;
		});
		
		// Setup Columns
		$(window).load(
		    function() {
		       app.setupColumns();
		    }
		);
	},
	
	setupColumns : function(){
		//Main Map
		// app.mapInit();
		
		// Variables
		var windowWidth = $(window).width();
		var columnWidth = 300;
		var marginWidth = 23;
		var numOfColumns = Math.floor(windowWidth/(columnWidth + marginWidth));
		var columns = []; // keep track of column height
		var lowest = 0;
		var sideMarginWidth = (windowWidth%320)/2 + 10; // center column grid 

		// Debug
		// console.log("windowWidth: " + windowWidth + " numOfColumns: " + numOfColumns + " sideMarginWidth:" + sideMarginWidth);	
		
		// Setup margin on side to center grid
		$("#user_home").css({
			"left": sideMarginWidth + "px",
			"width": numOfColumns+(columnWidth*numOfColumns + marginWidth) + marginWidth
		});
				
		// Cycle through every post and distribute 
		$('.post-container').each(function() {
		   	var self = this;

			// For every column
		 	for(var i = 0; i < numOfColumns; i++){

				// Check which one is the lowest, default 0
				lowest = 0;
				for(var i = 0; i < numOfColumns; i++){
					if(columns[i] != null){
						if(columns[i].columnHeight < columns[lowest].columnHeight){
							lowest = i;
						}
					} else {
						// First run
						columns[i] = new Object();
						columns[i].columnHeight = 0;
						columns[i].postCount = 1;
					}
				}

				// Add post to that column
				$(self).css({
					"position" : "absolute",
					"top" :	columns[lowest].columnHeight + marginWidth*columns[lowest].postCount + "px", //margin-top 20px
					"left" : lowest*columnWidth + lowest*marginWidth + "px",
				})
				
				$(self).fadeIn();

				// Add on height
				columns[lowest].columnHeight += $(self).height();
				columns[lowest].postCount++;
				
				// Debug
				// console.log("Column " + lowest + " has height: " + columns[lowest].columnHeight + " and $(self).height(): " + $(self).height());
			}
		});
		
		// Make invididual posts modal boxes
		$(".modal-trigger").click(function(){
			var self = this;
			var href = $(self).attr('href');
			var modal = $(self).attr('modal');

			if(!$(self).hasClass("loaded"))
			{
				console.log("Reuesting: " + href);
				
				$(href).click(); //Simulate click

				$(self).addClass("loaded");
			}
			else
			{
				$(modal).modal();

				console.log("Post has already been loaded");
			}
		});
	},

	mapInit : function(){
		// Creating an object literal containing the properties
		var options = {
			zoom: 14, 
			center: new google.maps.LatLng(40.4446772, -79.9450306),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl:true,
			mapTypeControlOptions:{
				style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
				mapTypeIds: [ google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE ]
			},
			disableDefaultUI: true,
			navigationControl:true,
			navigationControlOptions:{
				position: google.maps.ControlPosition.TOP_RIGHT,
				style: google.maps.NavigationControlStyle.SMALL
			},
			scrollwheel:false,
			streetViewControl: true,
			backgroundColor: '#FFFFFF'
		};

		// Creating the map 
		if( map == null){
			map = new google.maps.Map(document.getElementById('post-modal-map'), options);			
		}

		// Initiliaze geocoder
		if(!geocoder){ 
			geocoder = new google.maps.Geocoder();
		}

		// Try W3C Geolocation (Preferred)
		if(navigator.geolocation) {
			browserSupportFlag = true;
			navigator.geolocation.getCurrentPosition(function(position) {
				initialLocation   = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);

				var infowindow    = new google.maps.Marker({
					map: map,
					position: initialLocation
				});

				map.setCenter(initialLocation);
			}, 
			function() {
				handleNoGeolocation(browserSupportFlag);
			});
		} else if (google.gears) {
			// Try Google Gears Geolocation
			browserSupportFlag = true;

			var geo            = google.gears.factory.create('beta.geolocation');

			geo.getCurrentPosition(function(position) {
				initialLocation   = new google.maps.LatLng(position.latitude,position.longitude);

				var infowindow    = new google.maps.Marker({
					map: map,
					position: initialLocation
				});

				map.setCenter(initialLocation);
			}, function() {
				handleNoGeoLocation(browserSupportFlag);
			});

		} else {
			// Browser doesn't support Geolocation
			browserSupportFlag = false;
			app.mapHandleNoGeolocation(browserSupportFlag);
		}
	},
	
	mapHandleNoGeolocation : function(errorFlag){
		if (errorFlag) {
			var content = 'Error: The Geolocation service failed.';
		} else {
			var content = 'Error: Your browser doesn\'t support geolocation.';
		}

		var options = {
			map: map,
			position: new google.maps.LatLng(60, 105),
			content: content
		};

		var infowindow = new google.maps.InfoWindow(options);
		map.setCenter(options.position);
	}
}



// Custom
$(function() {
	//fancy boxes
	app.basicFunctionality();
	
	// user main views
	app.userMainView();

});

//listen to window resize
$(window).bind("resize", callSetupColumns);
function callSetupColumns(){
	app.setupColumns()
}