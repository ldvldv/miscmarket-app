class Membership < ActiveRecord::Base
  # Setup accessible (or protected) attributes for your model
  self.table_name = :markets_users

  belongs_to :user
  belongs_to :market

  validates_numericality_of :user_id, :only_integer => true
  validates_numericality_of :market_id, :only_integer => true
  validates_numericality_of :role_id, :only_integer => true
end
