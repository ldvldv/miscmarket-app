class Type < ActiveRecord::Base
  # Protects all the attributes.
  attr_accessible

  has_many :posts
  has_many :MarketsCategories

  validates_uniqueness_of :name
end
