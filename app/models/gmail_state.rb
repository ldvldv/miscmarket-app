class GmailState < ActiveRecord::Base
    has_and_belongs_to_many :gmail_accounts
    # Protects all the attributes.
    attr_accessible

    validates :description, :presence => true
end
