class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable, :token_authenticatable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :omniauthable , :confirmable

  # Relations
  has_one :gmail_account
  has_many :posts
  has_many :locations
  has_many :tokens, :class_name=>"OauthToken",:order=>"authorized_at desc",:include=>[:client_application]
  has_many :memberships
  has_many :markets, :through => :memberships
  has_many :search_queries

  # Setup accessible (or protected) attributes for your model
  attr_accessible :first_name, :last_name, :email, :password, :password_confirmation, :remember_me, :fb_id

  # Validation
  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :email, :presence => true, :uniqueness => true

  before_save do
    self.email.downcase! if self.email
  end

  # return 1 = member , return 2 = not confirmed, return 3 = not member
  def memberOf(user_id, market_id)
    User.find_by_id(user_id).memberships.each do |membership|
      if membership.market_id == market_id
        if membership.confirmed_at
          return 1
        else
          return 2
        end
      end
    end

    return 3
  end

  # returns true if the current_user is part of all the input array of markets, otherwise false
  def verify_markets(markets)

    # Making sure that the markets array has at least one market in it.
    if markets.nil? or markets == []
        return false
    end
    
    # Ensuring that the current user is allowed to post to the specified
    # markets.
    for market in markets
      if !self.confirmed_market_ids.include?(market.to_i)
        return false
      end
    end
    
    return true
  end

  # Returns the list of categories for the markets that the user is allowed to see.
  def categories(params = {})
    @user_id = id

    @error = 0
    sql_condition = "SELECT c.id, c.name, mc.type_id, COUNT(p.id) FROM categories c, markets_categories mc, posts p, markets_posts mp WHERE c.id = mc.category_id AND mc.market_id IN ("

    sub_select = ''
    if params.has_key?("markets")
      markets = params[:markets]
      @error = verify_markets(markets)
      sub_select = get_array_without_brackets(markets)
    else
      sub_select = "SELECT mu.market_id FROM markets_users mu WHERE mu.user_id = #{@user_id}"
    end

    sql_condition = sql_condition + sub_select + ") AND p.id = mp.post_id AND p.category_id = c.id AND p.type_id = mc.type_id AND mp.market_id IN (" + sub_select + ") GROUP BY p.category_id, mc.type_id;"
    @categories = nil
    if @error == 0
        @categories = connection.execute(sql_condition)
    end

    return @categories, @error
  end

  # Returns the markets in which the user is a confirmed member.
  def confirmed_markets()
    return self.markets.where("markets_users.confirmed_at IS NOT NULL")
  end

  # Returns an array of market ids in which the user is a confirmed member.
  def confirmed_market_ids()
    market_ids = Membership.find(:all, :select => :market_id, :conditions => ("user_id = " + id.to_s + " AND confirmed_at IS NOT NULL"))
    markets = []
    for market_id in market_ids
        markets.push(market_id.market_id)
    end
    return markets
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["user_hash"]
        user.email = data["email"]
        user.first_name = data["first_name"]
        user.last_name = data["last_name"]
        user.fb_id = data["id"]
        user.fb_token = session['devise.facebook_data']['credentials']['token']
      end
    end
  end

  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = Hash.new
    data['token'] = access_token['credentials']['token']
    data['id'] = access_token['extra']['user_hash']['id']

    return data
  end

  def self.find_for_authentication(conditions)
    conditions[:email].downcase!
    super(conditions)
  end

  # def self.authenticate(email,password)
  #   user = User.find_by_email(email)
  #   if user && user.valid_password?(password)
  #     user
  #   else
  #     nil
  #   end
  # end

  # Token_authenticable
  def create_token
    self.reset_authentication_token!
  end

  def destroy_token
    self.authentication_token = nil
    self.save
  end

  def to_param
    first_name.blank? ? id : first_name
  end
end
