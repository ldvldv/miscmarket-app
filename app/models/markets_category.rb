class MarketsCategory < ActiveRecord::Base

  # Protects all the attributes.
  attr_accessible :market_id, :category_id, :type_id

  belongs_to :market
  belongs_to :category
  belongs_to :type

  validates_numericality_of :market_id, :only_integer => true
  validates_numericality_of :category_id, :only_integer => true
  validates_numericality_of :type_id, :only_integer => true
end
