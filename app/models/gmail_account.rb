class GmailAccount < ActiveRecord::Base
  attr_protected :user_id, :oauth_token, :oauth_secret, :state, :active
  belongs_to :user
  has_one :gmail_state

  validates :user_id, :presence => true, :uniqueness => true
  validates :email, :presence => true, :uniqueness => true
  validates :gmail_state_id, :presence => true
  validates_inclusion_of :active, :in => [true, false]
end
