class Post < ActiveRecord::Base
  
  # Relations
  belongs_to :user
  belongs_to :category
  belongs_to :type
  belongs_to :state
  has_one :location
  has_many :reposts
  has_many :post_visibilities
  has_many :markets, :through => :post_visibilities
  has_many :uploads

  # just include the Tanker module
  include Tanker

  tankit 'post_search_index' do
    indexes :title
    indexes :description
    indexes :price
    indexes :type_id
    indexes :category_id
    indexes :availability
    indexes :state_id
    indexes :user_id
  end

  # Returns all the the posts that are visible in |markets|
  scope :visible, lambda{ |markets| {
    :conditions => "id IN (SELECT markets_posts.post_id FROM markets_posts WHERE markets_posts.market_id IN (" + markets + "))"
    }}

  after_save :update_tank_indexes
  after_destroy :delete_tank_indexes

  # Accessible
  attr_accessible :title, :description, :price, :category_id, :state, :uploads_attributes, :type_id, :uploads, :market_ids, :state_id
  accepts_nested_attributes_for :uploads, :allow_destroy => true

  # Validation
  validates :title, :presence => true, :length => { :maximum => 140 }
  validates :description, :allow_blank => true, :length => { :maximum => 900 }
  validates_numericality_of :price,:allow_blank => true, :greater_than_or_equal_to => 0
  validates_numericality_of :type_id, :presence => true, :only_integer => true
  validates_numericality_of :category_id, :presence => true, :only_integer => true
  validates_numericality_of :user_id, :presence => true, :only_integer => true
  # TODO(aayushkumar): Currently address_id can be nil.  Will change once we
  # figure this out.
  validates_numericality_of :address_id, :allow_nil => true
  # Availability can currently be AVAILABLE, EXPIRED or UNAVAILABLE.
  validates_numericality_of :availability, :only_integer => true, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 3

  # validate :post_markets
 

  # returns true if the current_user is part of all the input array of markets, otherwise false
  # def verify_markets(markets, user)

  #   # Making sure that the markets array has at least one market in it.
  #   if markets.nil? or markets == []
  #       return false
  #   end
    
  #   # Ensuring that the current user is allowed to post to the specified
  #   # markets.
  #   for market in markets
  #     if !user.confirmed_market_ids.include?(market.to_i)
  #       return false
  #     end
  #   end
    
  #   return true
  # end

  # def post_markets
  #   if :category_id == 18
  #     errors[:base] << "This person is invalid because ..."
  #   else
  #     errors[:base] << "This person is weird because ..."
  #   end
  # end 

  default_scope :order => 'posts.updated_at DESC' 

  # Doesn't work, why?
  def thumbnail
    self.uploads.first.upload.url(:thumbnail)
  end
end
