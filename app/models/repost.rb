class Repost < ActiveRecord::Base
	attr_accessible :post_id
	belongs_to :post
    validates_numericality_of :post_id, :only_integer => true
end
