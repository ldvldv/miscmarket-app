class Upload < ActiveRecord::Base
  attr_accessible :post_id, :upload
  has_attached_file :upload,
  :storage => :s3,
  :s3_credentials => "#{Rails.root}/config/amazon_s3.yml",
  :path => "/:style/:filename",  :styles => { :small => "100x100>", :thumbnail => "300x300>", :large => "500x500>" }

  validates_attachment_presence :upload
  validates_attachment_size :upload, :less_than => 3.megabytes
end
