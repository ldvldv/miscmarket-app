class PostVisibility < ActiveRecord::Base
  # Setup accessible (or protected) attributes for your model
  self.table_name = :markets_posts

  attr_accessible

  belongs_to :post
  belongs_to :market

  validates_numericality_of :post_id, :only_integer => true
  validates_numericality_of :market_id, :only_integer => true
end
