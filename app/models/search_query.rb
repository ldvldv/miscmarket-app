class SearchQuery < ActiveRecord::Base
    belongs_to :user
    has_many :markets_searches
    has_many :markets, :through => :markets_searches

    validates_numericality_of :user_id, :presence => true,
                              :only_integer => true
end
