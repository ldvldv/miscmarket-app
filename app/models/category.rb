class Category < ActiveRecord::Base
  has_many :markets_categories
  has_many :markets, :through => :markets_categories
  has_many :posts
  belongs_to :type

  # Protects all the attributes.
  attr_accessible

  validates_uniqueness_of :name
end
