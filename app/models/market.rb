class Market < ActiveRecord::Base
  has_many :memberships
  has_many :markets_categories
  has_many :post_visibilities

  has_many :users, :through => :memberships
  has_many :posts, :through => :post_visibilities
  has_many :categories, :through => :markets_categories

  has_many :markets_searches
  has_many :search_queries, :through => :markets_searches

  attr_accessible :id, :name, :description

  # Validation.
  validates :name, :presence => true
end
