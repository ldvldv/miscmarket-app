class Message < ActionMailer::Base
  default :from => "L@cmu.edu"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.message.contact_post_owner.subject
  #
  
  def messenger( recipient, sender, message_content)     
    @message_content = message_content
    
    mail(:to => recipient, :from => sender, :subject => "MiscMarket Message")
  end
end
