# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120302061333) do

  create_table "app_users", :force => true do |t|
    t.string    "email"
    t.string    "password_hash"
    t.string    "password_salt"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "categories", :force => true do |t|
    t.string    "name"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  add_index "categories", ["name"], :name => "index_categories_on_name"

  create_table "gmail_accounts", :force => true do |t|
    t.integer  "user_id"
    t.string   "email"
    t.string   "oauth_token"
    t.string   "oauth_secret"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "gmail_state_id"
    t.boolean  "active"
  end

  add_index "gmail_accounts", ["user_id", "email"], :name => "index_gmails_on_user_id_and_email"

  create_table "gmail_states", :force => true do |t|
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "gmail_states", ["id"], :name => "index_gmail_states_on_id"

  create_table "locations", :force => true do |t|
    t.integer   "user_id"
    t.string    "address_1"
    t.string    "address_2"
    t.string    "city"
    t.string    "zipcode"
    t.string    "country"
    t.decimal   "latitude"
    t.decimal   "longitude"
    t.boolean   "available"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "markets", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "markets", ["name"], :name => "index_markets_on_title"

  create_table "markets_categories", :force => true do |t|
    t.integer  "market_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "type_id"
  end

  add_index "markets_categories", ["category_id", "market_id", "type_id"], :name => "index_on_all_foreign_keys"
  add_index "markets_categories", ["market_id"], :name => "index_markets_categories_on_market_id"
  add_index "markets_categories", ["type_id"], :name => "index_markets_categories_on_type_id"

  create_table "markets_posts", :id => false, :force => true do |t|
    t.integer "market_id"
    t.integer "post_id"
  end

  add_index "markets_posts", ["market_id"], :name => "index_markets_posts_on_market_id"
  add_index "markets_posts", ["post_id"], :name => "index_markets_posts_on_post_id"

  create_table "markets_searches", :force => true do |t|
    t.integer  "market_id"
    t.integer  "search_query_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "markets_users", :force => true do |t|
    t.integer   "market_id"
    t.integer   "user_id"
    t.integer   "role"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "email"
    t.string    "confirmation_token"
    t.timestamp "confirmed_at"
    t.timestamp "confirmation_sent_at"
  end

  add_index "markets_users", ["user_id", "confirmed_at", "market_id"], :name => "index_markets_users_on_user_id_and_confirmed_at_and_market_id"

  create_table "posts", :force => true do |t|
    t.integer  "user_id"
    t.integer  "address_id"
    t.integer  "category_id"
    t.string   "title"
    t.text     "description"
    t.integer  "availability"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "price"
    t.integer  "type_id"
    t.integer  "state_id"
  end

  add_index "posts", ["category_id", "type_id", "id"], :name => "index_posts_on_category_id_and_type_id_and_id"
  add_index "posts", ["updated_at", "id", "availability", "type_id"], :name => "index_posts_on_updated_at_and_id_and_availability_and_type_id"
  add_index "posts", ["updated_at", "id", "category_id", "availability", "type_id"], :name => "index_on_retrieving_posts_by_categories"
  add_index "posts", ["updated_at", "id", "user_id", "availability", "type_id"], :name => "index_on_retrieving_posts_by_user"

  create_table "reposts", :force => true do |t|
    t.integer  "post_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "reposts", ["post_id", "created_at"], :name => "index_reposts_on_post_id_and_created_at"

  create_table "search_queries", :force => true do |t|
    t.string   "query"
    t.integer  "user_id_searched"
    t.integer  "availability"
    t.integer  "category_id"
    t.integer  "type_id"
    t.integer  "state_id"
    t.integer  "user_id"
    t.integer  "page"
    t.integer  "per_page"
    t.boolean  "error"
    t.string   "result_json"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "states", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "types", ["id"], :name => "index_types_on_id"

  create_table "uploads", :force => true do |t|
    t.integer   "post_id"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.timestamp "upload_updated_at"
    t.integer   "upload_file_size"
    t.string    "upload_content_type"
    t.string    "upload_file_name"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                                :null => false
    t.string   "encrypted_password",     :limit => 128,                :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "last_name"
    t.string   "first_name"
    t.string   "fb_id"
    t.string   "fb_token"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "state"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
