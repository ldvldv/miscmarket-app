class FixColumnNameSearchedByToUserIdSearched < ActiveRecord::Migration
  def change
    rename_column :search_queries, :searched_by, :user_id_searched
  end
end
