class AddTypeToMarketsCategories < ActiveRecord::Migration
  def change
    add_column :markets_categories, :type, :integer
  end
end
