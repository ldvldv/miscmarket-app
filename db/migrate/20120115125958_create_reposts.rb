class CreateReposts < ActiveRecord::Migration
  def change
    create_table :reposts do |t|
      t.integer :post_id

      t.timestamps
    end
    add_index :reposts, [:post_id, :created_at]
  end
end
