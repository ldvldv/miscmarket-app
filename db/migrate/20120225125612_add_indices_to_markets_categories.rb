class AddIndicesToMarketsCategories < ActiveRecord::Migration
  def change
    add_index :markets_categories, [:category_id, :market_id, :type_id], :name => 'index_on_all_foreign_keys'
    add_index :markets_categories, [:market_id]
    add_index :markets_categories, [:type_id]
  end
end
