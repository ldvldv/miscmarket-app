class RemoveStateFromPosts < ActiveRecord::Migration
  def change
    remove_index :posts, :name => 'index_posts_on_updated_at_and_id_and_availability_and_type_id'
    remove_column :posts, :state
    add_index :posts, [:updated_at, :id, :availability, :type_id], :order => {:updated_at => :desc, :name => 'index_on_retrieving_posts'}
  end
end
