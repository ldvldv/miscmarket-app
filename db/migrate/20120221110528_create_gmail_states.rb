class CreateGmailStates < ActiveRecord::Migration
  def change
    create_table :gmail_states do |t|
      t.string :description

      t.timestamps
    end
    add_index :gmail_states, :id 
  end
end
