class FixColumnNameSearchQueriesIdToSearchQueryId < ActiveRecord::Migration
  def change
    rename_column :markets_searches, :search_queries_id, :search_query_id
  end
end
