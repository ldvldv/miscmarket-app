class RenameGmailAccountToGmailAccounts < ActiveRecord::Migration
  def change
    rename_table :gmail_account, :gmail_accounts
  end

end
