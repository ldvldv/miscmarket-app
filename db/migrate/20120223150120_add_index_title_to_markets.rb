class AddIndexTitleToMarkets < ActiveRecord::Migration
  def change
    add_index :markets, [:title]
  end
end
