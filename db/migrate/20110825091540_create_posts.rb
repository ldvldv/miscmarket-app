class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :user_id
      t.integer :address_id
      t.integer :category_id
      t.integer :favorites_id
      t.string :title
      t.text :description
      t.string :state
      t.integer :available

      t.timestamps
    end
  end
end
