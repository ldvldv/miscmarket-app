class CreateGmails < ActiveRecord::Migration
  def change
    create_table :gmails do |t|
      t.integer :user_id
      t.string :email
      t.string :oauth_token
      t.string :oauth_secret

      t.timestamps
    end
  add_index :gmails, [:user_id, :email]
  end
end
