class RemoveFavoritesIdFromPost < ActiveRecord::Migration
  def up
    remove_column :posts, :favorites_id
  end

  def down
    add_column :posts, :favorites_id, :integer
  end
end
