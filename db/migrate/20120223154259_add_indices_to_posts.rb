class AddIndicesToPosts < ActiveRecord::Migration
  def change
    add_index :posts, [:updated_at, :id, :availability, :type_id], :order => {:updated_at => :desc}
    add_index :posts, [:updated_at, :id, :category_id, :availability, :type_id], :name => 'index_on_retrieving_posts_by_categories', :order => {:updated_at => :desc}
    add_index :posts, [:updated_at, :id, :user_id, :availability, :type_id], :name => 'index_on_retrieving_posts_by_user', :order => {:updated_at => :desc}
  end
end
