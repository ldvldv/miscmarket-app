class AddDetailsToGmailAccounts < ActiveRecord::Migration
  def change
    add_column :gmail_accounts, :state, :integer

    add_column :gmail_accounts, :active, :boolean

  end
end
