class AddIndicesToMemberships < ActiveRecord::Migration
  def change
    add_index :markets_users, [:user_id, :confirmed_at, :market_id]
  end
end
