class AddIndicesToPostVisibility < ActiveRecord::Migration
  def change
    add_index :markets_posts, [:market_id]
    add_index :markets_posts, [:post_id]
  end
end
