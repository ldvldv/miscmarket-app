class CreateMarketsPosts < ActiveRecord::Migration
  def self.up
    create_table :markets_posts, :id => false do |t| 
      t.references :market
      t.references :post
    end
  end

  def self.down
    drop_table :markets_posts
  end
end
