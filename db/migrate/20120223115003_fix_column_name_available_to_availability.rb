class FixColumnNameAvailableToAvailability < ActiveRecord::Migration
  def change
    rename_column :posts, :available, :availability
  end

end
