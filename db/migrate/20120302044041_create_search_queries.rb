class CreateSearchQueries < ActiveRecord::Migration
  def change
    create_table :search_queries do |t|
      t.string :query
      t.integer :searched_by
      t.integer :availability
      t.integer :category_id
      t.integer :type_id
      t.integer :state_id
      t.integer :user_id
      t.integer :page
      t.integer :per_page
      t.boolean :error
      t.string :result_json

      t.timestamps
    end
  end
end
