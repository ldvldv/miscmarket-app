class RenameColumnFromTitleToNameInMarkets < ActiveRecord::Migration
 def change
   rename_column :markets, :title, :name
 end
end
