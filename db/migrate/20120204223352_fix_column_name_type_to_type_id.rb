class FixColumnNameTypeToTypeId < ActiveRecord::Migration
 def change
    rename_column :posts, :type, :type_id
    rename_column :markets_categories, :type, :type_id
  end
end
