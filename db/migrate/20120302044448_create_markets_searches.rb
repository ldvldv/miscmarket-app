class CreateMarketsSearches < ActiveRecord::Migration
  def change
    create_table :markets_searches do |t|
      t.integer :market_id
      t.integer :search_queries_id

      t.timestamps
    end
  end
end
