class RemoveMarketIdFromCategories < ActiveRecord::Migration
  def up
    remove_column :categories, :market_id
  end

  def down
    add_column :categories, :market_id, :integer
  end
end
