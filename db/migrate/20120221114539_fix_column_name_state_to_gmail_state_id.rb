class FixColumnNameStateToGmailStateId < ActiveRecord::Migration
  def change
    rename_column :gmail_accounts, :state, :gmail_state_id
  end

end
