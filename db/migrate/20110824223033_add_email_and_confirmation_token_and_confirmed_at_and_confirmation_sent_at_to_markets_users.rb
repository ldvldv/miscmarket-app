class AddEmailAndConfirmationTokenAndConfirmedAtAndConfirmationSentAtToMarketsUsers < ActiveRecord::Migration
  def change
    add_column :markets_users, :email, :string
    add_column :markets_users, :confirmation_token, :string
    add_column :markets_users, :confirmed_at, :datetime
    add_column :markets_users, :confirmation_sent_at, :datetime
  end
end
