class CreateMarketsUsers < ActiveRecord::Migration
  def self.up
    create_table :markets_users do |t|
      t.integer :market_id
      t.integer :user_id
      t.integer :role

      t.timestamps
    end
  end

  def self.down
    drop_table :markets_users
  end
end
