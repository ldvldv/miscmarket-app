class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.integer :user_id
      t.string :address_1
      t.string :address_2
      t.string :city
      t.string :zipcode
      t.string :country
      t.decimal :latitude
      t.decimal :longitude
      t.boolean :available

      t.timestamps
    end
  end
end
