class RemoveMarketIdFromPost < ActiveRecord::Migration
  def up
    remove_column :posts, :market_id
  end

  def down
    add_column :posts, :market_id, :integer
  end
end
