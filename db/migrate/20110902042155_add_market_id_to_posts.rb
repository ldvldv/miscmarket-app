class AddMarketIdToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :market_id, :integer
  end
end
